from exceptions import NoFuelError, BrokenEngineError
from dataclasses import dataclass
from abc import ABC, abstractmethod


class Vechile(ABC):
    def __init__(self, weight, type_of_mov, engine):
        self.weight = weight
        self.type_of_mov = type_of_mov
        self.engine = engine
    
    def travel(self, destination):
        print(f'Travelling to {destination}!')

    @abstractmethod
    def signal(self):
        pass

    def fuel(self):
        self.fueled = True

    def broke(self):
        self.engine.broken = True


@dataclass()
class Engine:
    def __init__(self, type_of_fuel, count_of_pistons):
        self.fuel_type = type_of_fuel
        self.count_of_pistons = count_of_pistons
    
        self.broken = False

    def broke(self):
        self.broken = True


class Car(Vechile):
    def __init__(self, weight, type_of_car, mark, count_of_wheels):
        super().__init__(weight, 'наземный', Engine('бензин', 4))

        self.type_of_car = type_of_car
        self.mark = mark
        self.count_of_wheels = count_of_wheels

        self.locked = False

    def signal(self, count_of_times=1):
        for _ in range(count_of_times):
            print('Beep!!')

    def travel(self, destination, distance=-1):
        if self.engine.broken:
            raise BrokenEngineError

        if distance != -1:
            if distance >= 1000:
                print(f"Can't travel to {destination}, because its too distant.")
            else:
                print(f'Driving to {destination}!')
        else:
            print(f'Trying to drive to {destination}.')
    
    def lock_the_doors(self):
        self.locked = True

    def open_the_doors(self):
        self.locked = False


class Plane(Vechile):
    def __init__(self, weight, count_of_wings, count_of_engines, count_of_passangers):
        super().__init__(weight, 'воздушный', Engine('авиакеросин', 6))

        self.count_of_wings = count_of_wings
        self.count_of_engines = count_of_engines
        self.count_of_passangers = count_of_passangers

        self.fueled = False

    def signal(self, airbase):
        print(f'Sending signal to base {airbase}...')
    
    def travel(self, destination):
        if self.engine.broken:
            raise BrokenEngineError

        if self.fueled:
            print(f'Flying to {destination}')
            self.fueled = False
        else:
            raise NoFuelError

    def kill(self):
        self.kills += 1
        print("Shot down another plane. I'm good...")


class Ship(Vechile):
    def __init__(self, weight, material, crew_amount, armed):
        super().__init__(weight, 'водный', Engine('мазут', 12))

        self.material = material
        self.crew_amount = crew_amount
        self.armed = armed
        self.sail_time = 0

        self.fueled = False
    
    def signal(self, x, y):
        print(f'[TO EARTH] OUR COORDINATES X: {x}, Y: {y}')

    def travel(self, destination, time):
        self.sail_time += time
        if self.engine.broken:
            raise BrokenEngineError
        if self.sail_time >= 155:
            self.fueled = False
        if time > 155:
            print(f"Can't sail to {destination}, because there are too much travel time.")
        else:
            if self.fueled:
                print(f'Sailing to {destination}...')
            else:
                raise NoFuelError

    def arm(self):
        self.armed = True

    def disarm(self):
        self.armed = False

    def attack(self):
        if self.engine.broken:
            raise BrokenEngineError

        if self.armed:
            print('Firing rockets FAU-2!!!')
        else:
            print('There no weapon.')


class MercedesBenz(Car):
    def __init__(self, avto_brand, type_of_drive, car_mileage):
        super().__init__(25, 'Легковая', 'Mercedez_Benz', 4)
       
        self.avto_brand = avto_brand
        self.type_of_drive = type_of_drive
        self.car_mileage = car_mileage

    def get_avto_brand(self):
        return self.avto_brand

    def get_type_of_drive(self):
        return self.type_of_drive

    def get_info(self):
        return (self.avto_brand, self.type_of_drive)

    def set_car_mileage(self, miles):
        self.car_mileage = miles


class RaptorF22(Plane):
    def __init__(self, nickname, color):
        super().__init__(8.2, 2, 1, 1)

        self.armed = True
        self.kills = 0
        self.nickname = nickname
        self.color = color
    
    def travel(self, destination, purporse='exploration'):
        if self.engine.broken:
            raise BrokenEngineError

        if self.fueled:
            if purporse == 'exploration':
                print(f'Flying to {destination} to explore an area.')
                self.fueled = True
            elif purporse == 'fighting':
                print(f'Flying to air battle near {destination}!')
                self.fueled = False
            else:
                print(f'Unkown purporse. Please, specify (exploration / fighting).')
        else:
            raise NoFuelError

    def get_nickname(self):
        return self.nickname
    
    def get_color(self):
        return self.color
    
    def get_info(self):
        return (self.nickname, self.color)


class Sy27(Plane):
    def __init__(self, nickname, color):
        super().__init__(10.0, 2, 1, 1)

        self.armed = True
        self.kills = 0
        self.nickname = nickname
        self.color = color

    def travel(self, destination, purporse='exploration'):
        if self.engine.broken:
            raise BrokenEngineError

        if self.fueled:
            if purporse == 'exploration':
                print(f'Flying to {destination} to explore an area.')
                self.fueled = False
            elif purporse == 'fighting':
                print(f'Flying to air battle near {destination}!')
                self.fueled = False
            else:
                print(f'Unkown purporse. Please, specify (exploration / fighting).')
        else:
            raise NoFuelError

    def get_nickname(self):
        return self.nickname

    def get_color(self):
        return self.color

    def get_info(self):
        return (self.nickname, self.color)


class J20(Plane):
    def __init__(self, nickname, color):
        super().__init__(11.1, 4, 1, 1)

        self.armed = True
        self.kills = 0
        self.nickname = nickname
        self.color = color

    def travel(self, destination, purporse='exploration'):
        if self.engine.broken:
            raise BrokenEngineError

        if self.fueled:
            if purporse == 'exploration':
                print(f'Flying to {destination} to explore an area.')
                self.fueled = False
            elif purporse == 'fighting':
                print(f'Flying to air battle near {destination}!')
                self.fueled = False
            else:
                print(f'Unkown purporse. Please, specify (exploration / fighting).')
        else:
            raise NoFuelError

    def get_nickname(self):
        return self.nickname

    def get_color(self):
        return self.color

    def get_info(self):
        return (self.nickname, self.color)
