from exceptions import NoFuelError, BrokenEngineError
from classes import MercedesBenz, Ship, RaptorF22


if __name__ == '__main__':
    print('Тест Мерседеса!\n')

    car1 = MercedesBenz('Mercedes', 'леворульная', 15)
    print("Бренд автомобиля: ", car1.get_avto_brand(), "\nРассположение руля: ", car1.get_type_of_drive(), "\nПробег автомобиля: ", car1.car_mileage, "\n")
    car1.lock_the_doors()
    car1.travel('Норильск', 150)
    car1.signal()
    print("Пробег автомобиля: ", car1.set_car_mileage(150))
    car1.travel('Калининград', 1100)
    car1.broke()
    car1.open_the_doors()

    try:
        car1.travel('Москву', 150)
    except BrokenEngineError as error:
        print(error)

    print('\nКонец теста Мерседеса!')
    print('------------------------')

    print('\nТест Раптора!')

    plane1 = RaptorF22("Star", "Серый")
    print('Количесво Двигателей: ', plane1.count_of_engines, '\nЭкипаж: ', plane1.count_of_passangers, '\nКоличество крыльев: ', plane1.count_of_wings)
    plane1.fuel()
    plane1.travel("Средизменое море", 'exploration')
    plane1.signal("Bravo")
    print("Количество убийств до срожения: ", plane1.kills)
    plane1.travel("Индийский океан", 'fighting')
    plane1.kill()
    print("Количество убийств после срожения: ", plane1.kills)

    try:
        plane1.travel("Средизменое море", 'exploration')
    except NoFuelError as fullerror:
        print(fullerror)

    print('\nКонец теста Раптора!')
    print('------------------------')

    print('\nТест Судна!')

    ship1 = Ship(150, 'Сплав титан-алюминий', 150, False)
    ship1.fuel()
    print("Способ перемещения: ", ship1.type_of_mov, "\nВодоизмещение: ", ship1.weight, "\nТип топлтва: ", ship1.engine.fuel_type)
    print("Пройденное время до рейса: ", ship1.sail_time)
    ship1.travel("Индийский океан", 7)
    ship1.signal(05.5, 82.5)
    ship1.arm()
    ship1.travel("Индийский океан", 14)
    ship1.signal(02.5, 64.5)
    ship1.attack()
    print("Пройденное время после рейса: ", ship1.sail_time)
    ship1.broke()

    try:
       ship1.travel("Egypt", 15)
    except BrokenEngineError as error:
      print(error)
