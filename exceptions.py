class NoFuelError(BaseException):
    def __str__(self):
        return 'Not enough fuel.'


class BrokenEngineError(BaseException):
    def __str__(self):
        return 'The engine is broken.'
